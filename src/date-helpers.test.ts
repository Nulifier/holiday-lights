import { expect, test } from "vitest";
import { convertGeneralDate, isWithinGeneralInterval } from "./date-helpers";
import { formatDate, parseDate } from "./helpers";

test("it should check a regular interval", () => {
	const inter = { start: "10-Nov", end: "26-Dec" };
	expect(
		isWithinGeneralInterval(parseDate("01-Oct-2020"), inter)
	).toBeFalsy();
	expect(
		isWithinGeneralInterval(parseDate("10-Nov-2020"), inter)
	).toBeTruthy();
	expect(
		isWithinGeneralInterval(parseDate("10-Dec-2020"), inter)
	).toBeTruthy();
	expect(
		isWithinGeneralInterval(parseDate("25-Dec-2020"), inter)
	).toBeTruthy();
	expect(
		isWithinGeneralInterval(parseDate("26-Dec-2020"), inter)
	).toBeTruthy();
	expect(
		isWithinGeneralInterval(parseDate("27-Dec-2020"), inter)
	).toBeFalsy();
	expect(
		isWithinGeneralInterval(parseDate("01-Jan-2020"), inter)
	).toBeFalsy();
	expect(
		isWithinGeneralInterval(parseDate("01-Feb-2020"), inter)
	).toBeFalsy();
	expect(
		isWithinGeneralInterval(parseDate("01-Mar-2020"), inter)
	).toBeFalsy();
	expect(
		isWithinGeneralInterval(parseDate("01-Jun-2020"), inter)
	).toBeFalsy();
});

test("it should check an interval that goes past a year", () => {
	const inter = { start: "10-Nov", end: "01-Mar" };
	expect(
		isWithinGeneralInterval(parseDate("01-Oct-2020"), inter)
	).toBeFalsy();
	expect(
		isWithinGeneralInterval(parseDate("10-Nov-2020"), inter)
	).toBeTruthy();
	expect(
		isWithinGeneralInterval(parseDate("10-Dec-2020"), inter)
	).toBeTruthy();
	expect(
		isWithinGeneralInterval(parseDate("25-Dec-2020"), inter)
	).toBeTruthy();
	expect(
		isWithinGeneralInterval(parseDate("26-Dec-2020"), inter)
	).toBeTruthy();
	expect(
		isWithinGeneralInterval(parseDate("27-Dec-2020"), inter)
	).toBeTruthy();
	expect(
		isWithinGeneralInterval(parseDate("01-Jan-2020"), inter)
	).toBeTruthy();
	expect(
		isWithinGeneralInterval(parseDate("01-Feb-2020"), inter)
	).toBeTruthy();
	expect(
		isWithinGeneralInterval(parseDate("01-Mar-2020"), inter)
	).toBeTruthy();
	expect(
		isWithinGeneralInterval(parseDate("02-Mar-2020"), inter)
	).toBeFalsy();
	expect(
		isWithinGeneralInterval(parseDate("01-Jun-2020"), inter)
	).toBeFalsy();
});

test("it should convert general dates", () => {
	expect(formatDate(convertGeneralDate("01-Jan", 2020))).toBe("01-Jan-2020");
	expect(formatDate(convertGeneralDate("23-Oct", 2021))).toBe("23-Oct-2021");
	expect(formatDate(convertGeneralDate(1, 2020))).toBe("01-Jan-2020");
	expect(formatDate(convertGeneralDate(42, 2020))).toBe("11-Feb-2020");
});
