import { isAfter, isBefore, isWithinInterval, setDayOfYear } from "date-fns";
import { parseDate } from "./helpers.js";

/**
 * Represents a date as either "dd-MMM", eg. 01-Jan or as a number of days within the year.
 * 1 would be January 1.
 */
export type GeneralDate = string | number;
export interface GeneralInterval {
	start: GeneralDate;
	end: GeneralDate;
}

/**
 * Converts a GeneralDate into a specific date.
 * @param date The GeneralDate to convert.
 * @param year The year to put the date in.
 * @returns A specific date.
 */
export function convertGeneralDate(date: GeneralDate, year: number) {
	if (typeof date === "number") {
		const yearStart = new Date(year, 0);
		return setDayOfYear(yearStart, date);
	} else {
		return parseDate(`${date}-${year}`);
	}
}

/**
 * Checks if a specific date is within a GeneralInterval.
 */
export function isWithinGeneralInterval(date: Date, interval: GeneralInterval) {
	let start = convertGeneralDate(interval.start, date.getFullYear());
	let end = convertGeneralDate(interval.end, date.getFullYear());

	if (isAfter(start, end)) {
		if (isBefore(date, start)) {
			// Then we should use the previous year's start date
			start = convertGeneralDate(interval.start, date.getFullYear() - 1);
		} else {
			// Then we should use next year's end date
			end = convertGeneralDate(interval.end, date.getFullYear() + 1);
		}
	}

	return isWithinInterval(date, { start, end });
}
