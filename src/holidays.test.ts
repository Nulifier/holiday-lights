import { beforeEach, describe, expect, test } from "vitest";
import { isSameDay } from "date-fns";
import { hockeyTeamOverride, Preset } from ".";
import { parseDate } from "./helpers";
import { HolidayLights } from "./holidays";
import { TeamAbbreviation } from "@nulifier/hockey-api";

const data = [
	/* eslint-disable no-multi-spaces */
	["01-Jan-2020", Preset.NewYears],
	["02-Jan-2020", Preset.Christmas],
	["05-Jan-2020", Preset.Christmas],
	["10-Jan-2020", Preset.Christmas],
	["15-Jan-2020", Preset.Christmas],
	["20-Jan-2020", Preset.Christmas],
	["25-Jan-2020", Preset.Christmas],
	["30-Jan-2020", Preset.Christmas],
	["31-Jan-2020", Preset.Christmas],
	["01-Feb-2020", Preset.Christmas],
	["05-Feb-2020", Preset.Christmas],
	["10-Feb-2020", Preset.Christmas],
	["15-Feb-2020", Preset.Christmas],
	["20-Feb-2020", Preset.Christmas],
	["25-Feb-2020", Preset.Christmas],
	["29-Feb-2020", Preset.Christmas],
	["01-Mar-2020", Preset.Christmas],
	["02-Mar-2020", Preset.Default],
	["05-Mar-2020", Preset.Default],
	["10-Mar-2020", Preset.Default],
	["15-Mar-2020", Preset.Default],
	["17-Mar-2020", Preset.StPatricks],
	["20-Mar-2020", Preset.Default],
	["25-Mar-2020", Preset.Default],
	["30-Mar-2020", Preset.Default],
	["01-Apr-2020", Preset.Default],
	["05-Apr-2020", Preset.Default],
	["10-Apr-2020", Preset.Default],
	["12-Apr-2020", Preset.Easter],
	["15-Apr-2020", Preset.Default],
	["20-Apr-2020", Preset.Default],
	["25-Apr-2020", Preset.Default],
	["30-Apr-2020", Preset.Default],
	["01-May-2020", Preset.Default],
	["05-May-2020", Preset.Default],
	["10-May-2020", Preset.Default],
	["15-May-2020", Preset.Default],
	["20-May-2020", Preset.Default],
	["25-May-2020", Preset.Default],
	["30-May-2020", Preset.Default],
	["01-Jun-2020", Preset.Default],
	["05-Jun-2020", Preset.Default],
	["10-Jun-2020", Preset.Default],
	["15-Jun-2020", Preset.Default],
	["20-Jun-2020", Preset.Default],
	["25-Jun-2020", Preset.Default],
	["30-Jun-2020", Preset.Default],
	["01-Jul-2020", Preset.CanadaDay],
	["05-Jul-2020", Preset.Default],
	["10-Jul-2020", Preset.Default],
	["15-Jul-2020", Preset.Default],
	["20-Jul-2020", Preset.Default],
	["25-Jul-2020", Preset.Default],
	["30-Jul-2020", Preset.Default],
	["01-Aug-2020", Preset.Default],
	["05-Aug-2020", Preset.Default],
	["10-Aug-2020", Preset.Default],
	["15-Aug-2020", Preset.Default],
	["20-Aug-2020", Preset.Default],
	["25-Aug-2020", Preset.Default],
	["30-Aug-2020", Preset.Default],
	["01-Sep-2020", Preset.Default],
	["05-Sep-2020", Preset.Default],
	["10-Sep-2020", Preset.Default],
	["15-Sep-2020", Preset.Default],
	["20-Sep-2020", Preset.Default],
	["25-Sep-2020", Preset.Default],
	["30-Sep-2020", Preset.Default],
	["01-Oct-2020", Preset.Default],
	["05-Oct-2020", Preset.Default],
	["16-Oct-2020", Preset.Default],
	["17-Oct-2020", Preset.Halloween],
	["20-Oct-2020", Preset.Halloween],
	["25-Oct-2020", Preset.Halloween],
	["30-Oct-2020", Preset.Halloween],
	["31-Oct-2020", Preset.Halloween],
	["01-Nov-2020", Preset.Default],
	["05-Nov-2020", Preset.Default],
	["10-Nov-2020", Preset.Default],
	["11-Nov-2020", Preset.RemembranceDay],
	["15-Nov-2020", Preset.Default],
	["18-Nov-2020", Preset.Christmas],
	["20-Nov-2020", Preset.Christmas],
	["25-Nov-2020", Preset.Christmas],
	["30-Nov-2020", Preset.Christmas],
	["01-Dec-2020", Preset.Christmas],
	["05-Dec-2020", Preset.Christmas],
	["10-Dec-2020", Preset.Christmas],
	["15-Dec-2020", Preset.Christmas],
	["20-Dec-2020", Preset.Christmas],
	["24-Dec-2020", Preset.Christmas],
	["25-Dec-2020", Preset.Christmas],
	["26-Dec-2020", Preset.Christmas],
	["30-Dec-2020", Preset.Christmas],
	["31-Dec-2020", Preset.NewYears],
	/* eslint-enable no-multi-spaces */
];

describe("with no disabled holidays", () => {
	let lights: HolidayLights;
	beforeEach(() => {
		lights = new HolidayLights();
	});

	test.each(data)('for %s it should get "%s"', async (date, expected) => {
		expect(await lights.getHoliday(parseDate(date))).toBe(expected);
	});
});

describe("with some disabled holidays", () => {
	const overrides = {
		"17-Mar-2020": Preset.Default,
		"01-Jul-2020": Preset.Default,
	} as Record<string, Preset>;

	const testData = data.map((datum) => {
		if (datum[0] in overrides) return [datum[0], overrides[datum[0]]];
		return datum;
	});

	let lights: HolidayLights;
	beforeEach(() => {
		lights = new HolidayLights({
			disabledHolidays: [Preset.StPatricks, Preset.CanadaDay],
		});
	});

	test.each(testData)('for %s it should get "%s"', async (date, expected) => {
		expect(await lights.getHoliday(parseDate(date))).toBe(expected);
	});
});

test("it should accept an override function", async () => {
	const lights = new HolidayLights({
		overridePreset: (date: Date, preset: Preset) =>
			isSameDay(date, parseDate("01-Dec-2020")) ? Preset.Hockey : preset,
	});

	expect(await lights.getHoliday(parseDate("01-Dec-2020"))).toBe(
		Preset.Hockey
	);
});

test("it should accept a hockey override function", async () => {
	const lights = new HolidayLights({
		overridePreset: hockeyTeamOverride(TeamAbbreviation.Oilers),
	});

	expect(await lights.getHoliday(parseDate("01-Nov-2021"))).toBe(
		Preset.Hockey
	);
});
