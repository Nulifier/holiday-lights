import { format, parse } from "date-fns";
import * as days from "./holiday-days.js";

export type PartialBy<T, K extends keyof T> = Omit<T, K> & Partial<Pick<T, K>>;

export type DaysKeys = keyof typeof days;

export function getAllHolidays(year: number) {
	return Object.keys(days).reduce((obj, key) => {
		obj[key as DaysKeys] = (
			days[key as DaysKeys] as (year: number) => Date
		)(year);
		return obj;
	}, {} as Record<DaysKeys, Date>);
}

export function formatDate(date: Date) {
	return format(date, "dd-MMM-yyyy");
}

export function parseDate(date: string) {
	return parse(date, "dd-MMM-yyyy", new Date());
}
