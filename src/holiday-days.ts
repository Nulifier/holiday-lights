import { add, nextMonday, nextThursday } from "date-fns";

export function newYears(year: number) {
	return new Date(year, 0, 1);
}

export function stPatricks(year: number) {
	return new Date(year, 2, 17);
}

export function easter(year: number) {
	// Golden number - 1
	const G = year % 19;
	const C = Math.floor(year / 100);
	// Related to Epact
	const H = (C - Math.floor(C / 4) - Math.floor(((8 * C) + 13) / 25) + (19 * G) + 15) % 30;
	// Number of days from Mar 21 to the Paschal full moon
	const I = H - (Math.floor(H / 28) * (1 - (Math.floor(29 / (H + 1)) * Math.floor((21 - G) / 11))));
	// Weekday for the Paschal full moon
	const J = (year + Math.floor(year / 4) + I + 2 - C + Math.floor(C / 4)) % 7;
	// number of days from 21 March to the Sunday on or before the Paschal full moon
	const L = I - J;
	const month = 3 + Math.floor((L + 40) / 44);
	const day = L + 28 - 31 * Math.floor(month / 4);

	return new Date(year, month - 1, day);
}

export function canadaDay(year: number) {
	return new Date(year, 6, 1);
}

export function canadianThanksgiving(year: number) {
	const monthStart = new Date(year, 9, 1);
	return nextMonday(nextMonday(add(monthStart, { days: -1 })));
}

export function americanThanksgiving(year: number) {
	let d = add(new Date(year, 10, 1), { days: -1 });
	for (let i = 0; i < 4; ++i) {
		d = nextThursday(d);
	}
	return d;
}

export function halloween(year: number) {
	return new Date(year, 9, 31);
}

export function remembranceDay(year: number) {
	return new Date(year, 10, 11);
}

export function christmasEve(year: number) {
	return new Date(year, 11, 24);
}

export function christmas(year: number) {
	return new Date(year, 11, 25);
}

export function boxingDay(year: number) {
	return new Date(year, 11, 26);
}

export function newYearsEve(year: number) {
	return new Date(year, 11, 31);
}
