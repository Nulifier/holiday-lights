import { expect, test } from "vitest";
import * as helpers from "./helpers";

test("it should return an object with all the holidays", () => {
	const days = helpers.getAllHolidays(2020);
	expect(typeof days).toBe("object");
	expect(Object.keys(days).length).toBeGreaterThan(1);
	expect(Object.keys(days)).toContain("christmas");
	expect(days.christmas.toDateString()).toBe("Fri Dec 25 2020");
});

test("it should format dates", () => {
	expect(helpers.formatDate(new Date(2020, 0, 1))).toBe("01-Jan-2020");
});

test("it should parse dates", () => {
	expect(helpers.parseDate("01-Jan-2020").toDateString()).toBe(
		"Wed Jan 01 2020"
	);
});
