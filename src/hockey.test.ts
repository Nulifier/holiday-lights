import { expect, test } from "vitest";
import { hockeyTeamOverride } from "./hockey";
import { Preset } from "./holidays";
import { TeamAbbreviation } from "@nulifier/hockey-api";

test("it should override correctly with a name", async () => {
	const over = hockeyTeamOverride(TeamAbbreviation.Oilers);
	expect(await over(new Date(2021, 9, 19), Preset.Christmas)).toBe(
		Preset.Hockey
	);
	expect(await over(new Date(2021, 9, 20), Preset.Christmas)).toBe(
		Preset.Christmas
	);
});
