import { isTeamPlaying } from "@nulifier/hockey-api";
import { TeamAbbreviation } from "@nulifier/hockey-api";
import { Preset } from "./holidays.js";

/**
 * Override function for the HolidayLights system.
 */
export function hockeyTeamOverride(team: TeamAbbreviation) {
	return async (date: Date, preset: Preset) => {
		if (await isTeamPlaying(team, date)) {
			return Preset.Hockey;
		}

		return preset;
	};
}
