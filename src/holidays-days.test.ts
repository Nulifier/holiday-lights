import { expect, test } from "vitest";
import {
	americanThanksgiving,
	boxingDay,
	canadaDay,
	canadianThanksgiving,
	christmas,
	christmasEve,
	easter,
	halloween,
	newYears,
	newYearsEve,
	remembranceDay,
	stPatricks,
} from "./holiday-days";

test("it calculates New Year's Day", () => {
	const d = newYears(2020);
	expect(d.toDateString()).toBe("Wed Jan 01 2020");
});

test("it calculates St Patrick's Day", () => {
	const d = stPatricks(2020);
	expect(d.toDateString()).toBe("Tue Mar 17 2020");
});

test("it calculates Easter", () => {
	expect(easter(2020).toDateString()).toBe("Sun Apr 12 2020");
	expect(easter(2021).toDateString()).toBe("Sun Apr 04 2021");
	expect(easter(2022).toDateString()).toBe("Sun Apr 17 2022");
	expect(easter(2023).toDateString()).toBe("Sun Apr 09 2023");
});

test("it calculates Canada Day", () => {
	const d = canadaDay(2020);
	expect(d.toDateString()).toBe("Wed Jul 01 2020");
});

test("it calculates Canadian Thanksgiving", () => {
	const d = canadianThanksgiving(2020);
	expect(d.toDateString()).toBe("Mon Oct 12 2020");
});

test("it calculates Halloween", () => {
	const d = halloween(2020);
	expect(d.toDateString()).toBe("Sat Oct 31 2020");
});

test("it calculates Remembrance Day", () => {
	const d = remembranceDay(2020);
	expect(d.toDateString()).toBe("Wed Nov 11 2020");
});

test("it calculates American Thanksgiving", () => {
	const d = americanThanksgiving(2020);
	expect(d.toDateString()).toBe("Thu Nov 26 2020");
});

test("it calculates ChristmasEve", () => {
	const d = christmasEve(2020);
	expect(d.toDateString()).toBe("Thu Dec 24 2020");
});

test("it calculates Christmas", () => {
	const d = christmas(2020);
	expect(d.toDateString()).toBe("Fri Dec 25 2020");
});

test("it calculates Boxing Day", () => {
	const d = boxingDay(2020);
	expect(d.toDateString()).toBe("Sat Dec 26 2020");
});

test("it calculates New Year's Eve", () => {
	const d = newYearsEve(2020);
	expect(d.toDateString()).toBe("Thu Dec 31 2020");
});
