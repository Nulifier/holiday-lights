import { expect, test } from "vitest";
import * as index from "./index";

test("it should should have many exports", () => {
	expect(Object.keys(index).length).toBeGreaterThan(1);
});
