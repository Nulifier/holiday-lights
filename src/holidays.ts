import { isSameDay } from "date-fns";
import { GeneralInterval, isWithinGeneralInterval } from "./date-helpers.js";
import { DaysKeys, getAllHolidays, PartialBy } from "./helpers.js";

export enum Preset {
	Default = "Default",
	NewYears = "New Year's",
	StPatricks = "St Patrick's",
	Easter = "Easter",
	CanadaDay = "Canada Day",
	Halloween = "Halloween",
	RemembranceDay = "Remembrance Day",
	Christmas = "Christmas",
	Hockey = "Hockey",
}

export interface HolidayLightsOptions {
	overridePreset?: (date: Date, preset: Preset) => Preset | Promise<Preset>;
	disabledHolidays?: Preset[];
	/**
	 * The range of days to have christmas lights on if nothing else is happening, inclusive.
	 */
	christmas: GeneralInterval;
	halloween: GeneralInterval;
}

export class HolidayLights {
	private opts: HolidayLightsOptions;
	private holidayYear: number;
	private holidays: Record<DaysKeys, Date>;

	constructor(
		opts: PartialBy<HolidayLightsOptions, "christmas" | "halloween"> = {}
	) {
		this.holidayYear = new Date().getFullYear();
		this.holidays = getAllHolidays(this.holidayYear);

		// Assign some defaults
		this.opts = {
			christmas: { start: "18-Nov", end: "01-Mar" },
			halloween: { start: "17-Oct", end: "31-Oct" },
			...opts,
		};
	}

	async getHoliday(date: Date): Promise<Preset> {
		this.checkCache(date);

		let preset = Preset.Default;

		// Inteval holidays
		if (
			this.isHolidayEnabled(Preset.Christmas) &&
			isWithinGeneralInterval(date, this.opts.christmas)
		) {
			preset = Preset.Christmas;
		}

		if (
			this.isHolidayEnabled(Preset.Halloween) &&
			isWithinGeneralInterval(date, this.opts.halloween)
		) {
			preset = Preset.Halloween;
		}

		// Check if there is an override
		if (this.opts.overridePreset) {
			const result = Promise.resolve(
				this.opts.overridePreset(date, preset)
			);
			preset = await result;
		}

		// Single day holidays
		if (
			this.isHolidayEnabled(Preset.NewYears) &&
			isSameDay(date, this.holidays.newYears)
		) {
			preset = Preset.NewYears;
		}

		if (
			this.isHolidayEnabled(Preset.StPatricks) &&
			isSameDay(date, this.holidays.stPatricks)
		) {
			preset = Preset.StPatricks;
		}

		if (
			this.isHolidayEnabled(Preset.Easter) &&
			isSameDay(date, this.holidays.easter)
		) {
			preset = Preset.Easter;
		}

		if (
			this.isHolidayEnabled(Preset.CanadaDay) &&
			isSameDay(date, this.holidays.canadaDay)
		) {
			preset = Preset.CanadaDay;
		}

		// Single day Halloween override
		if (
			this.isHolidayEnabled(Preset.Halloween) &&
			isSameDay(date, this.holidays.halloween)
		) {
			preset = Preset.Halloween;
		}

		if (
			this.isHolidayEnabled(Preset.RemembranceDay) &&
			isSameDay(date, this.holidays.remembranceDay)
		) {
			preset = Preset.RemembranceDay;
		}

		// Single day christmas override
		if (
			this.isHolidayEnabled(Preset.Christmas) &&
			isSameDay(date, this.holidays.christmas)
		) {
			preset = Preset.Christmas;
		}

		if (
			this.isHolidayEnabled(Preset.NewYears) &&
			isSameDay(date, this.holidays.newYearsEve)
		) {
			preset = Preset.NewYears;
		}

		return preset;
	}

	isHolidayEnabled(holiday: Preset) {
		// If no holidays are disabled, then its enabled
		if (!this.opts.disabledHolidays) {
			return true;
		}
		return !this.opts.disabledHolidays.includes(holiday);
	}

	checkCache(date: Date) {
		// Make sure our cached holidays are correct
		if (date.getFullYear() !== this.holidayYear) {
			this.holidayYear = date.getFullYear();
			this.holidays = getAllHolidays(this.holidayYear);
		}
	}
}

export default HolidayLights;
