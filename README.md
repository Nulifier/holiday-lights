# @nulifier/holiday-lights

Determines the holiday lighting preset for a specific date.

This is used to integrate my [WLED](https://github.com/Aircoookie/WLED) controller with
[Home Assistant](https://www.home-assistant.io/) though [Node-RED](https://nodered.org/).

## How to Use

  * Add the [flow file](node-red/flow.json) to your Node-RED instance. The important code is located
    in the Determine Preset node.
  * Make sure to update the IP address of your WLED device in the Setup node.
  * Label your presets so they align with the values in the Preset enum in
    [holidays.ts](src/holidays.ts)
